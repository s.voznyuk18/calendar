import React from 'react'

import { useDispatch, useSelector } from 'react-redux'
import { deleteEvent } from '@/store/eventsSlice';
import { toggleModal } from '@/store/modalSlice';
import { deleteData } from '@/utils/api';

const DeleteEvent = () => {

  const {params: {_id}} = useSelector(state => state.modal);

  const dispatch = useDispatch();

  const handleDeleteEvent = async() => {
    const res = await deleteData('events', _id);

    if(res?.success) {
      dispatch(deleteEvent(res?.result));
      dispatch(toggleModal({
        isOpenModal: false,
        modalType: '',
        params: {}
      }))
    }
  }

  return (
    <>
       <div>DeleteEvent</div>
       <button onClick={() => handleDeleteEvent()}>
        DeleteEvent
       </button>
    </>
 
  )
}

export default DeleteEvent