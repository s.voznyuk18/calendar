import React from 'react';
import { yupResolver } from "@hookform/resolvers/yup";
import TextField from "@mui/material/TextField";
import { useForm, Controller } from "react-hook-form";
import { useDispatch } from 'react-redux';

import { validationSchema } from '@/utils/validation';
import { convertToDuration } from '@/utils';
import { postData } from '@/utils/api';
import styles from './AddEvent.module.css'
import { addEvent } from '@/store/eventsSlice';
import { toggleModal } from '@/store/modalSlice';

const AddEvent = () => {

  const dispatch = useDispatch();
  const { control, handleSubmit,  formState: { errors } } = useForm({
    mode: 'all',
    resolver:yupResolver(validationSchema),
    defaultValues: {
      startTime: "",
      endTime: "",
      endTime: "",
      title: "",
    },
  })

  const onSubmit = async (data) => {
    const config = {
      start: convertToDuration('08:00',  data?.startTime), 
      duration: convertToDuration(data?.startTime,  data?.endTime),
      title: data?.title, 
    }
    const event = await postData('events',config);
    if(event?.success) {
      dispatch(addEvent(event?.result));
      dispatch(toggleModal({
        isOpenModal: false,
        modalType: '',
        params: {}
      }))
    }
  }

  return (
    <> 
      <h1>Add new Event</h1>
      <form onSubmit={handleSubmit(onSubmit)} className={styles.form}>
        <Controller
          control={control}
          name="startTime"
          render={({ field }) => (
            <TextField
            type="time"
            label="startTime"
            error={errors?.startTime?.message}
            helperText={errors?.startTime?.message}
            style = {{width: 240}}
              {...field}
            />
          )}
        />
        <Controller
          control={control}
          name="endTime"
          render={({ field }) => (
            <TextField
            type="time"
            label="endTime"
            error={errors?.endTime?.message}
            helperText={errors?.endTime?.message}
            style = {{width: 240}}
              {...field}
            />
          )}
        />
        <Controller
          control={control}
          name="title"
          render={({ field }) => (
            <TextField
            type="text"
            label="title"
            error={errors?.title?.message}
            helperText={errors?.title?.message}
            style = {{width: 240}}
              {...field}
            />
          )}
        />
        <button type='submit' >add event</button>
      </form>
    </>
  )
}

export default AddEvent