'use client';

import React, { useEffect, useRef } from 'react';
import { createPortal } from 'react-dom';
import { useDispatch, useSelector } from 'react-redux';
import Image from 'next/image';


import AddEvent from './Modals/AddEvent';
import DeleteEvent from './Modals/DeleteEvent';
import styles from './Modal.module.css'

import { toggleModal } from '@/store/modalSlice';

const RenderModal = ({ modalType, ...props }) => {
  switch (modalType) {
    case 'addNewEvent':
      return <AddEvent/>
    case 'deleteEvent':
      return <DeleteEvent/>
    default:
      return <h1>Default</h1>
  }
}
const Modal = () => {

  const isMounted = useRef(false);

  useEffect(() => {
    isMounted.current = true;
    return () => { isMounted.current = false }
  }, [])

  const { isOpenModal, modalType, params } = useSelector((state) => state.modal);
  const dispatch = useDispatch();

  const handleCloseModal = () => {
    dispatch(toggleModal({  
      isOpenModal: false, 
      modalType: '', 
      params: {}
    }));
  };

  useEffect(() => {
    const handleKeyDown = (e) => {
      if (e.keyCode === 27 || e.key === "Escape") {
        handleCloseModal();
      }
    }

    document.addEventListener('keydown', handleKeyDown);

    return () => {
      document.removeEventListener("keydown", handleKeyDown);
    };

  }, [isOpenModal])

  const handleBackgroudClick = (e) => {
    if (e.target === e.currentTarget) handleCloseModal()
  }

  return (isOpenModal && isMounted?.current) ? createPortal(
    <div 
      className={`${styles.modal_wrapper} ${isOpenModal && styles.modal_wrapper_active}`} 
      onClick={handleBackgroudClick}
    >
      <div className={styles.modal_content}>
        <div
          className={styles.close}
          onClick={() => handleCloseModal()}
        >
          <Image
            width="18"
            height="18"
            src='/icon_close.svg'
            alt='close_icon'
          />
        </div>
        <RenderModal isOpenModal={isOpenModal} modalType={modalType} handleCloseModal={handleCloseModal} />
      </div>
    </div>,
    document.getElementById("modal-root")
  ) : null
}

export default Modal;