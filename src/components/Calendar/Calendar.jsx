'use client'
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { formatedTimeArray } from '@/utils';
import { toggleModal } from '@/store/modalSlice';
import { setEvents } from '@/store/eventsSlice';
import Schedule from '@/components//Schedule/Schedule';
import styles from './Calendar.module.css';

const Calendar = ({ events }) => {

  const time = formatedTimeArray('8:00', '17:00', 5)

  const dispatch = useDispatch();

  const handleOpenModal = (modalType, params) => {
    dispatch(toggleModal({
      isOpenModal: true,
      modalType,
      params
    }))
  }

  useEffect(() => {
    dispatch(setEvents(events));
  }, [events])

  return (
    <>
      <ul className={styles.calendar}>
        {<Schedule events={events} handleOpenModal={handleOpenModal} />}
        {
          time &&
          time.map((item, index) => {
            if (item.minutes % 30 === 0) {
              return (
                <li key={index} className={styles.calendar_cell}>
                  <div className={styles.calendar_cell_title}>{item.time}</div>
                  <div className={styles.calendar_cell_event} onClick={() => handleOpenModal('addNewEvent', {})}>
                  </div>
                </li>
              );
            }
          })
        }
      </ul >
    </>
  )
}

export default Calendar;
