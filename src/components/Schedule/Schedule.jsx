import Event from "@/components//Event/Event";
import { findOverlaps } from "@/utils";

import { useSelector } from "react-redux";

const Schedule = ({ handleOpenModal, events }) => {
  const storeEvents = useSelector(state => state?.events?.events)
  const currentEvents = storeEvents.length > 0 ? storeEvents : events;
  const overlaps = findOverlaps(currentEvents);

  return (
    <>
      {currentEvents.map((event, index) => (
        <Event
          key={index}
          event={event}
          overlapped={overlaps[index]}
          index={index}
          handleOpenModal={handleOpenModal}
        />
      ))}
    </>
  );
};

export default Schedule;
