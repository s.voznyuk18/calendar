import styles from './Event.module.css';

const Event = ({ event, overlapped, handleOpenModal }) => {

  const convertDurationToPx = (duration, maxHeight) => (maxHeight / 540) * duration;

  const style = {
    top: `${convertDurationToPx(event.start, 1080)}px`,
    height: `${convertDurationToPx(event.duration, 1080)}px`,
  }

  return (
    <div
      key={event?._id}
      style={style}
      className={styles.event}
      data-overlapped={overlapped ? 'overlapped' : null}
      onClick={() => handleOpenModal('deleteEvent', { _id: event?._id })}
    >
      {event.title}
    </div>
  );
};

export default Event;
