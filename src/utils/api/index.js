const BASE_URL = process.env.BASE_URL;

export const  getData = async (url) => {
  try {
    const res = await fetch(`${BASE_URL}/api/${url}`);

    if (!res.ok) {
      throw new Error('Failed to fetch data')
    }
   
    return res.json()
  } catch (error) {
    console.log(`error: `, error)
  }
}

export const postData = async(url, data) => {
  try {
    const response = await fetch(`${BASE_URL}/api/${url}`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });
    return await response.json();
  } catch(error) {
    console.log(`error: `, error)
  }
} 

export const deleteData = async(url, data) => {
  try {
    const response = await fetch(`${BASE_URL}/api/${url}`, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });
    return await response.json();
  } catch(error) {
    console.log(`error: `, error)
  }
} 