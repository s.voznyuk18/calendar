const convertToMilliseconds = (time) => {
  const minutes = time?.split(':')[0] * 60 + time?.split(':')[1] * 1;
  return minutes;
};

function convertTo12HourFormat(time24) {
  const timeParts = time24.split(":");
  const hours = parseInt(timeParts[0], 10);
  const minutes = parseInt(timeParts[1], 10);
  const period = hours >= 12 ? "PM" : "AM";
  const hours12 = hours % 12 || 12;

  return `${hours12}:${minutes < 10 ? "0" : ""}${minutes} ${period}`;
}

export const formatedTimeArray = (start, end, step) => {
  const timeInterval = [];
  const startTime = start;
  const allMinites = convertToMilliseconds(end) - convertToMilliseconds(start);
  const stepMilliseconds = step;

  for (let ts = 0; ts <= allMinites; ts += stepMilliseconds) {
    timeInterval.push(ts);
  }

  const intervalArray = timeInterval?.map((intervalItem) => {
    const minutes = intervalItem % 60;
    const hours = +startTime?.split(':')[0] + Math.trunc((intervalItem / 60) % 24);

    return {
      minutes: intervalItem,
      time: convertTo12HourFormat(`${hours}:${minutes}`)
    }
  });

  return intervalArray;
};

  // Function for find intersections events
export const findOverlaps = (events) => {
  const overlaps = {};

  events && events.forEach((event1, index1) => {
    events && events.forEach((event2, index2) => {
      if (index1 !== index2) {
        if (
          (event1.start >= event2.start && event1.start < event2.start + event2.duration) ||
          (event2.start >= event1.start && event2.start < event1.start + event1.duration)
        ) {
          overlaps[index1] = true;
          overlaps[index2] = true;
        }
      }
    });
  });

  return overlaps;
};


export const convertToDuration = (startTime, endTime) => {
  const [startHours, startMinutes] = startTime.split(':').map(Number);
  const [endHours, endMinutes] = endTime.split(':').map(Number);

  const startTotalMinutes = startHours * 60 + startMinutes;
  const endTotalMinutes = endHours * 60 + endMinutes;

  return endTotalMinutes - startTotalMinutes;
} 