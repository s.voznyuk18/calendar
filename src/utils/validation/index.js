import * as yup from "yup";

export const validationSchema = yup.object().shape({
  startTime: yup.string().required("Required field").test({
    name: 'startTime',
    message: 'more than 8:00 and less than 17:00',
    test: (value) => {
      const hour = +value.split(':')[0];
      return (hour >= 8) && (hour < 17);
    }
  }),
  endTime: yup.string().required("Required field").test({
    name: 'endTime',
    message: 'more than 8:00 and less than 17:00',
    test: (value) => {
      const hour = +value.split(':')[0];
      return (hour >= 8) && (hour < 17);
    }
  }),
  title: yup.string().required("Required field")
});