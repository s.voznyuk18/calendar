import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  events: [],
}

const eventsSlice = createSlice({
  name: 'events',
  initialState,
  reducers: {
    addEvent: (state, payload) => {
      state.events =[...state.events, payload?.payload];
    },
    setEvents: (state, payload) => {
      state.events = payload?.payload;
    },
    deleteEvent: (state, payload) => {
      const filteredEvents = state.events.filter(event => event?._id !== payload?.payload);
      state.events = filteredEvents;
    }
  }
});

export const {addEvent, setEvents, deleteEvent} = eventsSlice.actions;
export default eventsSlice.reducer;