import { configureStore } from "@reduxjs/toolkit";

import modalReducer from "./modalSlice";
import evetsReducer from "./eventsSlice"

export const store  = configureStore({
  reducer: {
    modal: modalReducer,
    events: evetsReducer
  }

});