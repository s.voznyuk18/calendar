import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isOpenModal: false, 
  modalType: '', 
  params: {}
}

const modalSlice = createSlice({
  name: 'modal',
  initialState,
  reducers: {
    toggleModal: (state, payload) => {
      // console.log(payload?.payload?.)
      state.isOpenModal = payload?.payload?.isOpenModal;
      state.modalType = payload?.payload?.modalType;
      state.params = payload?.payload?.params;
    }
  }
});

export const {toggleModal} = modalSlice.actions;
export default modalSlice.reducer;