import { getData } from "@/utils/api";

import styles from "./page.module.css";
import Calendar from "@/components/Calendar/Calendar";

export default async function Home() {

  const events = await getData('events');

  return (
    <main className={styles.main}>
      <Calendar events={events?.result}/>
    </main>
  );
}
