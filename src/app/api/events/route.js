import { NextResponse, NextRequest } from "next/server";
import { connectMongoDB } from "@/lib/mongodb";

import {Event} from "@/models/eventModel";

export async function GET() {
  await connectMongoDB();
  const events = await Event.find({}, "");

  if (!events) {
    return NextResponse.json({ success: false, msg: "Failed to get events" }, {
      status: 500
    })
  }
  return NextResponse.json({ success: true, result: events });
}

export async function POST(req) {
  const event = await req.json();

  await connectMongoDB();

  const addedEvent = await Event.create(event)

  if (!addedEvent) {
    return NextResponse.json({ success: false, msg: "Failed to Create event" }, {
      status: 500
    })
  }

  return NextResponse.json({ success: true, result: addedEvent }, {
    status: 200, headers: {
      'Access-Control-Allow-Headers': 'Content-Type, Authorization',
    }
  })
}

export async function DELETE(req) {
  const _id = await req.json();

  await connectMongoDB();

  const deletedEvent = await Event.deleteOne({ _id });

  if (deletedEvent?.deletedCount === 0) {
    return NextResponse.json({ success: false, msg: "Failed to delete event" }, {
      status: 500
    })
  }

  return NextResponse.json({ success: true, result: _id }, {
    status: 200, headers: {
      'Access-Control-Allow-Headers': 'Content-Type, Authorization',
    }
  })
}