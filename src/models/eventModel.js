import mongoose, { Schema } from "mongoose";

export const eventSchema = new Schema(
  {
    start: { type: Number, required: true },
    duration: { type: Number, required: true },
    title: { type: String, required: true },
  },
  {
    timestamps: true,
  }
);


export const Event = mongoose.models.Event || mongoose.model('Event', eventSchema);